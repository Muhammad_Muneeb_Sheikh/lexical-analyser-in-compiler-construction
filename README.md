# Lexical analyser in compiler construction

AOA. In this program we can generate tokens, by spliting the whole text/coding, which contain ClassPart,ValuePart and LineNumber.
for example: if we run this text as input
"void main()\n" +
"{int a=659,b,c,d;\n" +
"while(a<==a!=b&&&c)\n" +
"if else{\n" +
"string *ptr=\"abc++--bc\n" +
"|=d\n" +
"return;\n" +
"\"in\"t\n" +
"char a='\\n'+'+'+a+b!";

program gives an output like this
"void main()\n" +
"{int a=659,b,c,d;\n" +
"while(a<==a!=b&&&c)\n" +
"if else{\n" +
"string *ptr=\"abc++--bc\n" +
"|=d\n" +
"return;\n" +
"\"in\"t\n" +
"char a='\\n'+'+'+a+b!";